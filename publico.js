const code = `
(() => {
  let publico;
  Object.defineProperty(window, "Publico", {
    get() {
      return publico;
    },
    set(p) {
      publico = p;
      publico.BodyIsClean = true;
    },
    enumerable: false,
  });
})();
`;

const script = document.createElement("script");
script.src = `data:application/javascript,${encodeURIComponent(code)}`;
document.documentElement.append(script);
script.remove();

document.addEventListener('copy', e => e.stopPropagation(), true);
